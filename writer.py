#!/usr/bin/python

import numpy as np

'''
The instance of this class stores the data about the writer such as:
	id:				writer id
 	var_parameters: optimized parameters
'''
class Writer:
	def __init__(self, writer_id, var_parameters, features):
		self.id = writer_id
		self.var_parameters = var_parameters
		self.features = features
		if (features is not None):
			self.labels = np.full((features.shape[0]),writer_id,dtype=int)
		self.EER = 0
		self.EER_random = 0
		self.EER_userthresholds = 0
		self.EER_userthresholds_random = 0
		self.FRR = 0
		self.FAR_simple = 0
		self.FAR_random = 0
		self.FAR_skilled = 0
		self.AUC = 0
		self.AUC_random = 0
		self.svmStats = None
		self.genuinePreds = 0
		self.randomPreds = 0
		self.simplePreds = 0
		self.skilledPreds = 0
		self.genuineIndices = 0
		self.random_test_indices = 0

	def getWriterId(self):
		return self.writer_id

	def getFeatures(self):
		return self.features

	def getLabels(self):
		return self.labels

	def getVarParameters(self):
		return self.var_parameters

	def setVarParameters(self, newParameters):
		self.var_parameters = newParameters

	def getAvg_EER(self):
		return np.mean(np.asarray(self.EER))

	def getAvg_EER_userthresholds(self):
		return np.mean(np.asarray(self.EER_userthresholds))

	def getAvg_EER_userthresholds_random(self):
		return np.mean(np.asarray(self.EER_userthresholds_random))

	def getAvg_FRR(self):
		return np.mean(np.asarray(self.FRR))

	def getAvg_FAR_random(self):
		return np.mean(np.asarray(self.FAR_random))

	def getAvg_FAR_simple(self):
		return np.mean(np.asarray(self.FAR_simple))

	def getAvg_FAR_skilled(self):
		return np.mean(np.asarray(self.FAR_skilled))

	def getAvg_AUC(self):
		return np.mean(np.asarray(self.AUC))

	def getAvg_AUC_random(self):
		return np.mean(np.asarray(self.AUC_random))
