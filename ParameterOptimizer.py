
from abc import ABCMeta, abstractmethod
"""
Abstract class for the parameter optimizers
"""
class ParameterOptimizer:
	__metaclass__ = ABCMeta

	@abstractmethod
	def getAverageParameterVector(self):
		pass

	@abstractmethod
	def show_parameters(self):
		pass

	@abstractmethod
	def extractFeatures(self, images):
		pass

	@abstractmethod
	def evaluate_parameters(self, params):
		pass

	@abstractmethod
	def optimize_parameters(self, args):
		pass
