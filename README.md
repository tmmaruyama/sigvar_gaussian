# Intrapersonal Parameter Optimization Using a Gaussian Filter

This repository contains the code and instructions to use the intrapersonal variability model in [1] to optimize the parameters of a Gaussian filter. The Gaussian filter is used to generate synthetic samples of offline signatures directly in the feature space.

[1] T. M. Maruyama, L. S. Oliveira, A. S. Britto Jr., and R. Sabourin, "Intrapersonal parameter optimization for offline handwritten signature augmentation", IEEE Trans. Inf. Forensics Security, vol. 16, pp. 1335-1350, 2021.  http://dx.doi.org/10.1109/TIFS.2020.3033442 ([preprint](https://arxiv.org/abs/2010.06663))

Topics:

* [Installation](#installation): How to set-up the dependencies / download the models to extract features and generate the synthetic samples
* [Usage](#usage): How to use this code to optimize the parameters of the Gaussian filter using the GPDS-960 dataset

# Installation

* This code was developed to run in Ubuntu 16.04 and Ubuntu 18.04.
* Therefore, it was not tested using another operating system.

## Pre-requisites

The code is written in Python 2.7<sup>1</sup>. We recommend using the Anaconda python distribution ([link](https://www.anaconda.com/products/individual#Downloads)), and create a new environment using:
```
conda create -n sigvar -y python=2
conda activate sigvar
```

The following libraries are required

* Scipy version 0.18
* Pillow version 3.0.0
* OpenCV
* Theano<sup>2</sup>
* Lasagne<sup>2</sup>
* DEAP

They can be installed by running the following commands:

```
pip install scipy==0.18.0
pip install pillow==3.0.0
conda install -y matplotlib
pip install opencv-python==4.2.0.32
pip install "Theano==0.9"
pip install https://github.com/Lasagne/Lasagne/archive/master.zip
pip install sklearn
pip install deap
```

## Downloading the CNN models

* Since the CNN SigNet-F is used to extract the features. First, we need to clone (or download) the repository:
https://github.com/luizgh/sigver_wiwd
* Copy the file ```preprocess\normalize.py``` to our ```preprocess``` folder
* Copy the files ```cnn_model.py``` and ```signet.py``` to our ```cnn_models``` folder
* Download the pre-trained CNN models from the [project page](https://www.etsmtl.ca/Unites-de-recherche/LIVIA/Recherche-et-innovation/Projets/Signature-Verification)
  * Save / unzip the models in the ```cnn_models``` folder

## Downloading the Sigvar model

* Download the [optimized parameters](https://drive.google.com/file/d/1uAYU62kUvj3fsEvWs1iQYN1riSUd09Gg/view?usp=sharing) (sigvar model) of the Gaussian Filter using 20 writers of GPDS-960 dataset
  * Save / unzip the sigvar model in the ```sigvar_models``` folder

## Testing

First, modify the following variables of the ```example_generate_synthetic_samples.py``` file according to your needs:
* **signature_path** defines a path to a signature image
* **cnn_path** defines a path to the pre-trained CNN SigNet-F model
* **sigvar_path** defines a path to the file with the sigvar model (optimized parameters)

Save your modifications in the ```example_generate_synthetic_samples.py``` file

Open a terminal, activate your environment and run ```example_generate_synthetic_samples.py```:
```bash
conda activate sigvar

python example_generate_synthetic_samples.py
```

# Usage

The following code (```run_optimization_example.sh```) shows how to optimize the parameters of Gaussian filter using the GPDS-960 dataset:

```bash
#!/bin/bash

# Remember to activate the environment before using the code
conda activate sigvar

# Run the parameter optimization code
python GaussianFilterParameterOptimizer.py \
        --data-path path/to/GPDS/dataset \
        --cnn-path path/to/cnn/cnn_model.pkl \
        --model-path path/to/optimized/parameters/sigvar_model.pkl \
        --num-duplicates 1 \
        --num-writers 20 \
        --num-iterations 20 \
        --num-particles 100 \
        --checkpoint path/to/checkpoint/suffix_to_a_file_
```

* **dataset-path** defines a path to the GPDS dataset
* **cnn-path** defines a path to the pretrained CNN model that is used to extract the signature features
* **model-path** defines a path to the file where the optimized parameter vectors will be stored
* **num-duplicates** is the number of duplicates per genuine sample that will be generated during the optimization process
* **num-writers** is the number of writers that will be used for optimization
* **num-iterations** is the maximum number of iterations that the optimization will be performed
* **num-particles** is the maximum number of particles (parameter vectors) that will be used during the optimization
* **checkpoint** defines a path and a suffix for the files that will store the optimized parameter vector per writer.
If the optimization unexpectly stops, the optimization will use the last values stored in the checkpoint files

# Citation

If you used our code, please cite the following papers:

[1] T. M. Maruyama, L. S. Oliveira, A. S. Britto Jr., and R. Sabourin, "Intrapersonal parameter optimization for offline handwritten signature augmentation", IEEE Trans. Inf. Forensics Security, vol. 16, pp. 1335-1350, 2021. http://dx.doi.org/10.1109/TIFS.2020.3033442 ([preprint](https://arxiv.org/abs/2010.06663))

[2] T. DeVries and G. W. Taylor, "Dataset augmentation in feature space", in Proc. 5th Int. Conf. Learn. Repr., 2017, pp. 1–12.

[3] F. Vargas, M. Ferrer, C. Travieso, and J. Alonso, "Off-line handwritten signature GPDS-960 corpus", in Proc. 9th Int. Conf. Document Anal. Recognit. (ICDAR), Sep. 2007, pp. 764–768. http://dx.doi.org/10.1109/ICDAR.2007.4377018

[4] L. G. Hafemann, R. Sabourin, and L. S. Oliveira, "Learning features for offline handwritten signature verification using deep convolutional neural networks", Pattern Recognition, vol. 70, pp. 163-176, 2017. http://dx.doi.org/10.1016/j.patcog.2017.05.012 ([preprint](https://arxiv.org/abs/1705.05787))


# License

The source code is released under the BSD 2-clause license. Note that the models used the GPDS dataset for training (which is restricted for non-comercial use).
