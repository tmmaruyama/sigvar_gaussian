
import numpy as np
import scipy
from sklearn import metrics

class ClusterMetrics:
	"""
	The instance of this class is used to compute the cluster metrics.
	"""

	def computeCentroid(self, cluster):
		"""
		Compute the centroid of a cluster. The centroid of a cluster is the
		average value of all the elements of the cluster.
			cluster: numpy array with the cluster elements. Each row is an
					element and column is a value of the element in the j th
					dimension
		"""
		return np.average(cluster, axis=0)

	def computeDistanceBetweenCentroids(self, cluster1, cluster2):
		"""
		Return the Euclidean distance between the centroids of the cluster1
		and cluster2.
			cluster1: numpy array with the elements of the cluster1. Each row is
					an element and column is a value of the element in the j th
					dimension.
			cluster2: numpy array with the elements of the cluster2. Each row is
					an element and column is a value of the element in the j th
					dimension.
		"""
		mi1 = np.average(cluster1, axis=0)
		mi2 = np.average(cluster2, axis=0)
		dist = scipy.spatial.distance.euclidean(mi1, mi2)
		return dist

	def computeCohesion(self, cluster):
		"""
		Compute the intraclass variability of a cluster using the cohesion.
		It measures the dispersion between the elements of the cluster and its
		centroid.
			cluster: numpy array with the cluster elements. Each row is an
					element and column is a value of the element in the j th
					dimension
		"""
		n_feat = cluster.shape[0]
		mi = np.average(cluster, axis=0)
		diff = cluster-mi
		cohesion = np.sum(np.square(diff))
		return cohesion

	def computeAbsoluteSilhouetteIndex(self, clusters, labels):
		"""
		Compute the absolute value of the silhouettte index between the clusters.
			clusters: numpy array with the cluster elements. Each row is an
					element and column is a value of the element in the j th
					dimension
			labels: list of labels. Each label determines in which cluster the
					element beleongs.
		"""
		return abs(metrics.silhouette_score(clusters, labels))
