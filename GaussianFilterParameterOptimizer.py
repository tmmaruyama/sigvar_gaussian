#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__	import print_function, division, unicode_literals

import numpy as np
import os
import sys
import re
import argparse
import datetime
import random
import traceback
from math import sqrt
import cv2
from cv2 import imwrite,imread
import operator
import random
import pickle
from six.moves import cPickle
from scipy.ndimage.filters import gaussian_filter1d
import sklearn
# Use deap library to implement PSO
from deap import base
from deap import benchmarks
from deap import creator
from deap import tools

from multiprocessing import Pool
import multiprocessing as mp

from preprocess.normalize import preprocess_signature

from cnn_models.cnn_model import CNNModel
from cnn_models import signet

from ParameterOptimizer import ParameterOptimizer
from GaussianFilterGenerator import GaussianFilterGenerator
from ClusterMetrics import ClusterMetrics
from writer import Writer

"""
This class implements the method proposed in [1] to optimize the parameters of
the Gaussian filter [2] to generate synthetic genuine signatures in the feature
space. It also uses the Handwritten Offline Signature Dataset called GPDS-960
[3], and the CNN Signet-F [4] to extract the signature features. For more
details, read the papers [1], [2], [3], and [4]. If you used this code, please
cite the following papers:

[1] T. M. Maruyama, L. S. Oliveira, A. S. Britto Jr., and R. Sabourin,
"Intrapersonal parameter optimization for offline handwritten signature
augmentation", IEEE Trans. Inf. Forensics Security, vol. 16, pp. 1335-1350,
2021.

[2] T. DeVries and G. W. Taylor, "Dataset augmentation in feature space",
in Proc. 5th Int. Conf. Learn. Repr., 2017, pp. 1–12.

[3] F. Vargas, M. Ferrer, C. Travieso, and J. Alonso, "Off-line handwritten
signature GPDS-960 corpus", in Proc. 9th Int. Conf. Document Anal.
Recognit. (ICDAR), Sep. 2007, pp. 764–768.

[4] L. G. Hafemann, R. Sabourin, and L. S. Oliveira, "Learning features for
offline handwritten signature verification using deep convolutional neural
networks", Pattern Recognition, vol. 70, pp. 163-176, 2017.
"""

class GaussianFilterParameterOptimizer(ParameterOptimizer):
	"""
	This class is used to optimize the parameter vectors of the Gaussian Filter.
	The Gaussian filter generates synthetic feature vectors based on the
	original ones, with the optimized parameter vectors.
	"""
	def __init__(self, args):
		signet_path, sigvar_path, canvas_size = args
		# it is necessary to add cwd to path when script run
		# by slurm (since it executes a copy)
		sys.path.append(os.getcwd())
		self.signet_path = signet_path
		self.sigvar_path = sigvar_path
		self.canvas_size = canvas_size  # Maximum signature size
		self.cnn = CNNModel(signet, signet_path)# CNN SigNet
		self.classifier = None
		self.scaler = None
		self.n_signatures = 6 # default
		self.sig_sample = None
		self.feat = None
		self.writers_var = []
		#print('type %d' % self.type)
		self.parameter_avg = None
		# It is used to compute the cluster metrics
		self.cm = ClusterMetrics()
		self.generator = GaussianFilterGenerator()
		# cluster data
		self.centroid = None # centroid of the cluster with genuine signatures
		self.cohesion = 0 # sparsity of the genuine signatures in the feature space

		# PSO is used to minimize the absolute silhouette index value
		creator.create("FitnessMin", base.Fitness, weights=(-1.0,))
		creator.create("Particle", np.ndarray, fitness=creator.FitnessMin, speed=list, smin=None, smax=None, best=None)
		if os.path.exists(self.sigvar_path):
			self.initWritersVar()

	def initWritersVar(self):
		"""
		Itinialize the variables from the previously saved files
		"""
		with open(self.sigvar_path, 'rb') as input:
			self.writers_var = cPickle.load(input)
		X = []
		y = []
		for w in self.writers_var:
			X.extend(w.features)
			y.extend(w.labels)
		#self.scaler = sklearn.preprocessing.StandardScaler(with_mean=False)
		#scaledtrainX = self.scaler.fit_transform(X)

		self.parameter_avg = self.getAverageParameterVector()

	def getAverageParameterVector(self):
		"""
		Compute the average parameter vector.
		"""
		if self.parameter_avg is None:
			writers = self.writers_var
			first_w = writers[0]
			parameter_vector_length = first_w.var_parameters.shape[0]
			parameters = np.array([]).reshape(0,parameter_vector_length)
			for w in self.writers_var:
				parameters = np.vstack([parameters, w.var_parameters])
			self.parameter_avg = np.mean(parameters, axis=0)
		return self.parameter_avg

	def show_parameters(self):
		"""
		Show the optimized parameter vectors and the average parameter vector
		"""
		if self.writers_var is not None:
			print("=======================================================================================")
			print("Parameter Vectors")
			print("---------------------------------------------------------------------------------------")
			for w in self.writers_var:
				print("Writer #%s: %s" % (w.id, w.var_parameters))
			print("=======================================================================================")
			print("Average Parameter vector: %s" % (str(self.getAverageParameterVector())))
			print("=======================================================================================")
		else:
			print("There is not parameters to show!")

	def loadSignatureImages(self, signature_path, writer):
		"""
		Load the genuine signature images of a writer.
		"""
		# List of signatures of a writer
		img_paths = sorted(os.listdir(signature_path[writer]))
		# List of genuine signatures of a writer
		genuine_files = filter(lambda x: x[0:2] == 'c-', img_paths)
		# Concatenate the path with the filenames and load the genuine signature images
		signatures = [imread(os.path.join(signature_path[writer],img_path),0) for img_path in genuine_files]
		return signatures

	def normalizeN(self, signatures, canvas_size):
		"""
		Normalize the signature images and return the normalized images.
		"""
		norm = [preprocess_signature(signature, canvas_size) for signature in signatures]
		return norm

	def extractFeatures(self, images):
		"""
		Extract the features from the images using CNN Signet-F and return them.
		"""
		# Normalize the signature images
		norm_dup = self.normalizeN(images, self.canvas_size)
		# extract the features from the normalized images
		return self.cnn.get_feature_vector_multiple(norm_dup)

	def optimize_parameters(self, args):
		"""
		Optimize the parameters of each writer present in GPDS 20DL.
		"""
		dataset_path, n_duplicates, n_writers, iterations, n_particles, checkpoint = args
		date_format = "%d-%m-%Y %H:%M:%S"
		writers = sorted(os.listdir(dataset_path))
		#20 previously randomly selected writers from DL that cover the dataset space
		val_writers = [431, 490, 503, 525, 588, 611, 631, 641, 643, 654, 673, 676, 701, 716, 797, 825, 897, 912, 935, 945]
		val_writers = [str(i) for i in val_writers]

		print("%s" % val_writers)
		wsignature_paths = {writer: (os.path.join(dataset_path, writer)) for writer in val_writers}

		X_total = np.empty((0, 2048))
		Xd_total = np.empty((0, 2048))
		y_total = []
		yd_total = []
		writer_var_path = self.sigvar_path#"writer_variability_var_10.pkl"
		t0 = datetime.datetime.now()
		output = open('sigvar_log_'+out+'.txt', 'w')
		output.write("=======================================================\n")
		#output.write("Scikit-learn version "+sklearn.__version__+"\n")
		#output.write("Sigvar 3\n")
		#output.write("Args: %s\n" % args)
		output.write("Start at %s\n" % t0.strftime(date_format))
		output.write("=======================================================\n")
		print("=======================================================\n")
		print("Scikit-learn version "+sklearn.__version__+"\n")
		#output.write("Sigvar 3\n")
		#output.write("Args: %s\n" % args)
		print("Start at %s\n" % t0.strftime(date_format))
		print("=======================================================\n")
		for w in val_writers:
			t0 = datetime.datetime.now()
			print("----------------------------------------------------------\n")
			print('Optimizing sigma values for writer #%s' % (w))
			output.write("----------------------------------------------------------\n")
			output.write('%s\n' % (t0.strftime(date_format)))
			output.write('Optimizing sigma values for writer #%s\n' % (w))
			output.flush()
			os.fsync(output.fileno())
			# Load the signature images
			signatures = self.loadSignatureImages(wsignature_paths, w)
			# Select the first 12 signatures for the writer
			w_signatures = signatures[:12]
			# search the best parameters from the writer
			pop, logbook, best = self.optimize_writer_parameters(w_signatures, n_duplicates, iterations, n_particles, checkpoint+str(w)+".pkl")
			# Verifies if the writer is in the writers_var
			writer_var = next((wv for wv in self.writers_var if int(wv.id) == int(w)), None)
			# Update the best saved parameters from the writer of writers_var
			if writer_var is not None:
				writer_var.setVarParameters(best)
			# Create a new writer object
			else:
				writer_var = Writer(w, best, self.feat)
				self.writers_var.append(writer_var)
			# Save the best parameters from the writer
			with open(writer_var_path ,'wb') as wfp:
				pickle.dump(self.writers_var, wfp)
			output.write('%s\n' % (logbook))
			output.write('Population %s\n' % (pop))
			output.write('Best [sigma_min sigma_max] %s\n' % (best))
			output.flush()
			os.fsync(output.fileno())
			tf = datetime.datetime.now()
			output.write('%s\n' % (tf.strftime(date_format)))
			dt = (tf-t0)
			output.write('Time to train the model: %s\n' % dt)
			output.flush()
			os.fsync(output.fileno())
			del signatures
		output.write("=======================================================\n")
		out2 = (datetime.datetime.now().strftime(date_format))
		output.write("End at %s\n" % out2)
		output.write("=======================================================\n")
		output.flush()
		os.fsync(output.fileno())
		output.close()
		print("=======================================================\n")
		print("End at %s\n" % out2)
		print("=======================================================\n")
		self.show_parameters()


	def optimize_writer_parameters(self, signatures, n_signatures, iterations, n_particles, checkpoint):
		"""
		For a writer, it find the best sigma_min and sigma_max values to
		increase the number of feature vectors. The number of feature vectors
		is increased applying a low-pass Gaussian filter in the original
		feature vector. The parameter sigma is defined selecting a random value
		between sigma_min and sigma_max following a uniform distribution.
		"""
		#signatures, n_signatures, iterations, n_particles, checkpoint = args
		#self.type = 1
		self.n_signatures = n_signatures
		cnn = self.cnn
		self.sig_sample = signatures
		self.scaler = sklearn.preprocessing.StandardScaler(with_mean=False)
		# Normalize genuine signatures and extract features from normalized signatures
		self.feat = self.scaler.fit_transform(self.extractFeatures(signatures))
		# Compute cluster metrics
		self.centroid = self.cm.computeCentroid(self.feat)
		self.cohesion = self.cm.computeCohesion(self.feat)
		cohesion_gen = self.cohesion

		y = np.ones(len(signatures))
		#self.classifier.fit(feat_sig, y)
		#self.plot_distribution(signatures, n_signatures)
		toolbox = base.Toolbox()
		#toolbox.register("map", pool.map)
		toolbox.register("particle", self.generateParticles, size=2, pmin=0.01, pmax=1.0, smin=0.001, smax=0.010)
		toolbox.register("update", self.update_particle, phi1=2, phi2=6)
		toolbox.register("evaluate", self.evaluate_parameters)
		toolbox.register("population", tools.initRepeat, list, toolbox.particle)
		toolbox.register("select", tools.selTournament, tournsize=3)
		if checkpoint:
			# A file name has been given, then load the data from the file
			print("Loading %s" % checkpoint)
			try:
				# Load the data of PSO
				with open(checkpoint, "r") as cp_file:
					cp = pickle.load(cp_file)
					pop = cp["population"]
					start_gen = cp["generation"]+1
					halloffame = cp["halloffame"]
					logbook = cp["logbook"]
					random.setstate(cp["rndstate"])
					best = halloffame[0]
			except :
				# Start a new PSO
				pop = toolbox.population(n=n_particles)
				start_gen = 0
				halloffame = tools.HallOfFame(maxsize=1, similar=np.array_equal)
				logbook = tools.Logbook()
				best = None
		else:
			# Start a new PSO
			pop = toolbox.population(n=n_particles)
			start_gen = 0
			halloffame = tools.HallOfFame(maxsize=1, similar=np.array_equal)
			logbook = tools.Logbook()
			best = None
		stats = tools.Statistics(lambda ind: ind.fitness.values)
		stats.register("avg", np.mean)
		stats.register("std", np.std)
		stats.register("min", np.min)
		stats.register("max", np.max)
		logbook.header = ["gen", "evals"] + stats.fields

		GEN = iterations
		np_best = np.zeros(2)
		best_dist = 10000
		best_si = 1.
		best_dco = 100000.
		best_gco = cohesion_gen
		best_diffco = 10000.
		agents = mp.cpu_count() -2
		chunksize = int(n_particles/agents)
		print("Cores=%s, Chunksize=%s, Population=%s" % (agents, chunksize, len(pop)))
		if start_gen < (GEN-1):
			for g in range(start_gen, GEN):
				# Create a pool of workers sized according to the CPU cores available.
				pool = Pool(processes=agents)
				fits = pool.map(self,pop, chunksize)
				pool.close()
				pool.join()
				#fits = [self.evaluate_parameters(p) for p in pop]
				# Update the local and the global best parameter vectors
				for fit, part in zip(fits, pop):
					#print("[[%s]-[%s]] SI: %s, Dist: %s, GCO: %s, DCO: %s, DiffCO: %s, evaluate %s" % (out,out2, si, centroid_dist, self.cohesion, cohesion_dup, cohesion_diff, params)
					#print('PART: %s, FITNESS: %s' %(part, fit))
					si, centroid_dist, cohesion_dup, cohesion_diff = fit
					part.fitness.values = (si,)
					# Update the local best parameter vector
					if part.best is None or (part.best.fitness < part.fitness):
						part.best = creator.Particle(part)
						part.best.fitness.values = part.fitness.values
						#print('LOCAL: PART: %s, FITNESS: %s' %(part, fit))
					# Update the global best parameter vector
					if best is None or (best.fitness < part.fitness):
						best = creator.Particle(part)
						best.fitness.values = part.fitness.values
						best_si = si
						best_dist = centroid_dist
						best_dco = cohesion_dup
						best_diffco = cohesion_diff
						#print('GLOBAL: PART: %s, Fitness[ SI: %f, DistC: %f, GCO: %f, DCO: %f, DIFFCO: %f]' %(part, best_si, best_dist, best_gco, best_dco, best_diffco))
				for part in pop:
					toolbox.update(part, best)
				fits = None
				halloffame.update(pop)
				record = stats.compile(pop)
				# Gather all the fitnesses in one list and print the stats
				logbook.record(gen=g, evals=len(pop), **stats.compile(pop))
				print(logbook.stream)
				pop = toolbox.select(pop, k=len(pop))
				# Fill the dictionary using the dict(key=value[, ...]) constructor
				cp = dict(population=pop, generation=g, halloffame=halloffame,
					logbook=logbook, rndstate=random.getstate())
				#print("Saving %s" % checkpoint)
				# Save a filte with the optimize parameters for the current writer
				with open(checkpoint, "wb") as cp_file:
					pickle.dump(cp, cp_file)
				#if (g+1)%5==0:
				#si, centroid_dist, cohesion_gen, cohesion_dup, cohesion_diff = best.fitness.values
				#print('Best: %s, Fitness[ SI: %f, DistC: %f, GCO: %f, DCO: %f, DIFFCO: %f]' % (best, best_si, best_dist, best_gco, best_dco, best_diffco))
				#sys.stdout.flush()
		best_si, best_dist, best_dco, best_diffco = self.evaluate_parameters(best)
		print('Best: %s' % (best))
		print('Fitness[ SI: %f, DistC: %f, GCO: %f, DCO: %f, DIFFCO: %f]' % (best_si, best_dist, best_gco, best_dco, best_diffco))
		np_best[0] = best[0]
		np_best[1] = best[1]
		return pop, logbook, np_best

	def generateParticles(self, size, pmin, pmax, smin, smax):
		"""
		Generate particles with the parameter vectors with the initial values.
		Each particle represents a parameter vector with sigma_min and sigma_max.
		Each particle has:
		- a velocity/speed part.speed
		- a minimum speed pat.smin
		- a maximum speed part.smax
		"""
		p0 = np.random.uniform(pmin, pmax)
		p1 = np.random.uniform(np.amax(p0), pmax)
		part = creator.Particle([p0, p1])

		part.smin = [-0.01, -0.01]
		part.smax = [0.01, 0.01]
		speed_min = [-0.01, -0.01]
		speed_max = [0.01, 0.01]
		s0 = random.choice(speed_min)
		s1 = random.choice(speed_max)
		part.speed = [s0, s1]
		#print('part %s' % part)
		return part

	def update_particle(self, part, best, phi1, phi2):
		"""
		Update the position of the particles that will be evaluated. It also
		updates their velocity/speed.
		"""
		c1 = (1+sqrt(5))/2#np.random.uniform(0, phi1, len(part))
		c2 = 1#np.random.uniform(4.1-np.amin(c1), phi2, len(part))

		chi = (3-sqrt(5))/2
		# Update de particle's velocity/speed
		part.speed = (np.multiply(chi, part.speed) + np.multiply(c1,(part.best - part)) + c2*(best - part))
		for i, speed in enumerate(part.speed):
			# keep the minimum value in the first position and maximum value in the last position
			if speed < part.smin[i]:
				part.speed[i] = part.smin[i]
			elif speed > part.smax[i]:
				part.speed[i] = part.smax[i]
		# Update the particle's position
		part += part.speed

	def evaluate_parameters(self, params):
		"""
		Verify if the original feature vectors are similar to the ones generated
		by the Gaussian Filter. To do that, two clusters are used: one with the
		feature vectors of the genuine signatures and another one with the
		feature vectors of the duplicates (samples generated by the Gaussian
		filter). The similarity is evaluated using the absolute value of the
		silhouette index abs(si). If abs(si) is 0 or close to 0, the two clusters
		are similar. If the abs(si) is close to 1, the two clusters are not
		similar. We can also analyze:
		- the distance between the centroids centroid_dist;
		- the variability of the cluster of duplicates cohesion_dup;
		- the difference between the variability of the two clusters
		cohesion_diff.
		"""
		try:
			# Since this method runs in parallel, it is used to generate diffent random values.
			# If it is not used, it will generate the same random value for all the parallel processes
			np.random.seed(None)

			date_format = "%d-%m-%Y %H:%M:%S"
			out = (datetime.datetime.now().strftime(date_format))
			sigmas = params
			#print('evaluate %s' % params)
			feats = np.copy(self.feat)
			duplicates = []
			# generate the new feature vectors applying a Gaussian filter
			n_new_vectors = 1
			feat_dup,_ = self.generator.generateSyntheticSamples(feats, sigmas, n_new_vectors)
			feats = np.concatenate((feats, feat_dup),axis=0)
			y = np.concatenate((np.ones(self.feat.shape[0]),np.full(feat_dup.shape[0],2,dtype=int)),axis=0)
			#COMPUTE CLUSTER METRICS
			# Compute distance between the cluster centroids
			centroid_dist = self.cm.computeDistanceBetweenCentroids(feats, feat_dup)
			# Compute absolute difference between the cohesions (sparsities)
			cohesion_dup = self.cm.computeCohesion(feat_dup)
			cohesion_diff = np.abs(self.cohesion-cohesion_dup)
			# Compute the Silhouette Index (It measures the distance between the clusters and their sparsity)
			# If Silhoutte index is equal to zero, both clusters are overlayed
			abs_si = self.cm.computeAbsoluteSilhouetteIndex(feats, y)
			out2 = (datetime.datetime.now().strftime(date_format))
			#print("[[%s]-[%s]] SI: %s, Dist: %s, GCO: %s, DCO: %s, DiffCO: %s, evaluate %s" % (out,out2, si, centroid_dist, self.cohesion, cohesion_dup, cohesion_diff, params))
			del feat_dup
			del feats
			#return (abs(si), centroid_dist, self.cohesion, cohesion_dup, cohesion_diff)
			return (abs_si,centroid_dist, cohesion_dup, cohesion_diff)
		except Exception as e:
			print('Caught exception in worker thread (sigmas = %s):' % sigmas)
			# This prints the type, value, and stack trace of the
			# current exception being handled.
			traceback.print_exc()
			print()
			raise e

	def generateSyntheticSamplesWithAverageParameterVector(self, samples, n_new_samples, with_original=False):
		"""
		Generate Feature vectors with correlated noise using the average of all
		the optimized parameter vectors. The correlated noise is generated using
		a low-pass gaussian filter in 1D. The sigma values are randomly
		determined considering a uniform distribution ranging from sigma_min to
		sigma_max (sigmas).

		:param samples: original feature vectors
		:param n_new_samples: number of new samples per feature vector
		:with_original: if it is True, it returns the original samples with the
						synthetic ones.	First, the synthetic samples generated
						for the first original sample, then the first original
						sample. Second, the synthetic samples generated for the
						second original sample, then the second original sample,
						and so on.
						if it is False, it returns only the synthetic samples.
						First, the synthetic samples generated for the first
						original sample. Second, the synthetic samples generated
						for the second original sample, and so on.

		:return:
		:	Feature vectors with noise
		:	Selected sigma values
		"""
		generator = self.generator
		sigmas = self.getAverageParameterVector()
		return generator.generateSyntheticSamples(samples, sigmas, n_new_samples, with_original=with_original)

	def __call__(self, params):
		return self.evaluate_parameters(params)

if __name__ == '__main__':
	#np.random.seed(8)
	mp.freeze_support()
	date_format = "%d-%m-%Y %H:%M:%S"
	parser = argparse.ArgumentParser(description='Optimize the parameters of the Gaussian filter to generate synthetic feature vectors.')
	parser.add_argument('--dataset-path', required=True) #Path to the dataset
	parser.add_argument('--cnn-path', required=True) #Path to pre-trained CNN Signet-F
	parser.add_argument('--model-path', required=True) #Path to the model with the optimized parameters
	parser.add_argument('--num-duplicates', required=True, default=1) #number of synthetic samples that are generated per original sample
	parser.add_argument('--num-writers', required=True, default=20) # number of writers that are selected to optimize the parameters
	parser.add_argument('--num-iterations', default=20) # number of the iterations used by the optimization method
	parser.add_argument('--num-particles', default=100) # number of parameter vectors that are evaluated per iteration
	parser.add_argument('--checkpoint', required=True) # Path to the temparary files that are created during the optimization.
	args = parser.parse_args()
	print('Arguments: %s' % args)
	t0 = datetime.datetime.now()
	out = (t0.strftime("%d-%m-%Y_%H_%M_%S"))
	print(t0.strftime(date_format))

	dataset_path = args.dataset_path
	cnn_path = args.cnn_path
	sigvar_path = args.model_path
	n_duplicates = int(args.num_duplicates)
	n_writers = int(args.num_writers)
	iterations = int(args.num_iterations)
	n_particles = int(args.num_particles)
	checkpoint = args.checkpoint
	# GPDS-960 canvas size (for more details read [1] and [4])
	canvas_size = (952, 1360)

	parameters1 = (cnn_path, sigvar_path, canvas_size)
	parameters2 = (dataset_path, n_duplicates, n_writers, iterations, n_particles, checkpoint)
	gfpo = GaussianFilterParameterOptimizer(parameters1)
	gfpo.optimize_parameters(parameters2)
