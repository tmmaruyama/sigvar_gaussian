
from abc import ABCMeta, abstractmethod

class Generator:
    __metaclass__ = ABCMeta

    @abstractmethod
    def generateSyntheticSamples(self, samples, params, n_new_vectors, with_original=False):
        pass
