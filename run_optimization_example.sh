#!/bin/bash

# Remember to activate the environment before using the code
conda activate sigvar

# Run the parameter optimization code
python GaussianFilterParameterOptimizer.py \
        --dataset-path path/to/GPDS/dataset \
        --cnn-path path/to/cnn/cnn_model.pkl \
        --model-path path/to/optimized/parameters/sigvar_model.pkl \
        --num-duplicates 1 \
        --num-writers 20 \
        --num-iterations 20 \
        --num-particles 100 \
        --checkpoint path/to/checkpoint/suffix_to_a_file_
