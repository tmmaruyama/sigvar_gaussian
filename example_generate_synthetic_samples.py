#!/usr/bin/python
from __future__	import print_function, division, unicode_literals

import numpy as np
import cv2
from cv2 import imread
from GaussianFilterParameterOptimizer import GaussianFilterParameterOptimizer

# Path to a signature image
signature_path = '/media/tmmaruyama/datasets/GPDS-ORIGINAL-MOISES/002/c-002-01.png'
# Path to CNN SigNet-F model
cnn_path = './cnn_models/signetf_lambda0.999.pkl'
# Path to the file with the optimized parameters
sigvar_path = './sigvar_models/sigvar_sigma_minmax_signetf_0999_20selectedDL.pkl'

# Load the signature image
signature_image = imread(signature_path,0)
# Number of synthetic feature vectors that will be generated using the Gaussian filter
n_new_samples = 10

# Canvas size used to normalize the signature images in GPDS-960
canvas_size = (952, 1360)
# Create a tuple of arguments
arguments = (cnn_path, sigvar_path, canvas_size)

# Create an object of the class GaussianFilterParameterOptimizer
# It loads the previously optimized parameters and the pre-trained CNN SigNet-F.
# The canvas_size is used in the feature extraction process to normalize the signature images.
optimizer = GaussianFilterParameterOptimizer(arguments)

# Show the optimized parameter vectors
optimizer.show_parameters()

# Extract the signature feature vectors using the CNN SigNet-F
original_feature_vectors = optimizer.extractFeatures([signature_image])
# Synthetic feature vectors generated using the Gaussian filter and the optimized parameters
synthetic_feature_vectors, selected_sigmas = optimizer.generateSyntheticSamplesWithAverageParameterVector(original_feature_vectors, n_new_samples)
print()
print("=======================================================================================")
print("----------------------------Original Feature Vectors-----------------------------------")
print(original_feature_vectors.shape)
print(original_feature_vectors)
print("---------------------------Synthetic Feature Vectors-----------------------------------")
print(synthetic_feature_vectors.shape)
print(synthetic_feature_vectors)
print("=======================================================================================")
