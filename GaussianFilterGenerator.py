
import os
import sys
import numpy as np
from scipy.ndimage.filters import gaussian_filter1d
from Generator import Generator

class GaussianFilterGenerator(Generator):
	def generateSyntheticSamples(self, samples, params, n_new_vectors, with_original=False):
		"""
		Generate Feature vectors with correlated noise. The correlated noise is
		generated using a low-pass gaussian filter in 1D. The sigma values are randomly
		determined considering a uniform distribution ranging from sigma_min to
		sigma_max.

		:param samples: original feature vectors
		:param params[0]: lower limit of the uniform distribution
		:param params[1]: higher limit of the uniform distribution
		:param n_new_vectors: number of new samples per feature vector
		:with_original: if it is True, it returns the original samples with the
						synthetic ones.	First, the synthetic samples generated
						for the first original sample, then the first original
						sample. Second, the synthetic samples generated for the
						second original sample, then the second original sample,
						and so on.

		:return:
		:	Feature vectors with noise
		:	Selected sigma values
		"""
		if n_new_vectors==0:
			return samples, []
		g_rows = samples.shape[0]
		sigmas = []
		sigma_min = params[0]
		sigma_max = params[1]
		if with_original:
			array_feat_noise = np.empty(((g_rows+g_rows*n_new_vectors),samples.shape[1]), dtype = np.float64)
		else:
			array_feat_noise = np.empty(((g_rows*n_new_vectors),samples.shape[1]), dtype = np.float64)
		row = 0
		for i in range(0, samples.shape[0]):
			for j in range(0, n_new_vectors):
				# Choose the sigma value randomly using an uniform distribution
				sigma = np.random.uniform(sigma_min, sigma_max)
				sigmas.append(sigma)
				feat_noise = gaussian_filter1d(samples[i], sigma=sigma).reshape(1,samples.shape[1]).astype(np.float64)
				array_feat_noise[row,:] = feat_noise
				row = row+1
			if with_original:
				array_feat_noise[row,:] = samples[i].reshape(1,samples.shape[1]).astype(np.float64)
				row = row+1
		return array_feat_noise, sigmas
